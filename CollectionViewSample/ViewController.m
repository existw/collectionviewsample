//
//  ViewController.m
//  CollectionViewSample
//
//  Created by Leon Wang on 2016/10/26.
//  Copyright © 2016年 Leon Wang. All rights reserved.
//

#import "ViewController.h"
#import "EventCollectionViewCell.h"

@interface ViewController () <UICollectionViewDelegate, UICollectionViewDataSource> {
    int COL_NUM;
}

@property (strong, nonatomic) NSArray *events;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    COL_NUM = 2;
    
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *plistPath = [bundle  pathForResource:@"events" ofType:@"plist"];
    
    self.events = [[NSArray alloc] initWithContentsOfFile:plistPath];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark implemntation data source
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    int num = [self.events count] % COL_NUM;
    if (num == 0) {
        return [self.events count] / COL_NUM;
    } else {
        return [self.events count] / COL_NUM + 1;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return COL_NUM;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    EventCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    NSDictionary *event = self.events[indexPath.section * COL_NUM + indexPath.row];
    
    cell.labelView.text = [event objectForKey:@"name"];
    cell.imageView.image = [UIImage imageNamed:event[@"image"]];
    
    return cell;
}

#pragma mark implemntation delegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *event = self.events[indexPath.section * COL_NUM + indexPath.row];
    
    NSLog(@"select event name: %@", event[@"name"]);
}

@end
