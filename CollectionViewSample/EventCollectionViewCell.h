//
//  EventCollectionViewCell.h
//  CollectionViewSample
//
//  Created by Leon Wang on 2016/10/26.
//  Copyright © 2016年 Leon Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *labelView;

@end
